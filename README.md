# Upstart

Upstart is an attempt to replace systemd as the default Ubuntu initializer, in all Ubuntu versions (target versions are 18.04 and up). Development and mainstream usage of Upstart was discontinued by Canonical in 2014; the purpose of this repo is to bring the project back to life.

**From the original Launchpad site:**

>>>
Upstart is an event-based replacement for the /sbin/init daemon which handles starting of tasks and services during boot, stopping them during shutdown and supervising them while the system is running.

It was originally developed for the Ubuntu distribution, but is intended to be suitable for deployment in all Linux distributions as a replacement for the venerable System-V init.
>>>

For more information, please see this [Wikipedia page](https://en.wikipedia.org/wiki/Upstart_(software)).

Feel free to fork this repository and make merge requests, issues, etc. I can also assign you to the project upon request (see issue #1).